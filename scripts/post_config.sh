#!/bin/bash -eux

# Install VBoxGuestAdditions
mkdir -p /mnt/cdrom
mount -t iso9660 -o loop /home/vagrant/VBoxGuestAdditions.iso /mnt/cdrom
sh /mnt/cdrom/VBoxLinuxAdditions.run
umount /mnt/cdrom
rm -f /home/vagrant/VBoxGuestAdditions.iso

# Update root ca-cert
if [ -f /home/vagrant/tls-ca-bundle.pem ]; then
   cat /home/vagrant/tls-ca-bundle.pem > /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
   rm -f /home/vagrant/tls-ca-bundle.pem
fi


