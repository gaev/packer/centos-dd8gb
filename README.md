# centos-dd8Gb

Configuration scripts having generated the Vagrant box https://app.vagrantup.com/GAEV/boxes/centos7-dd8Gb

This box was built from a server install image of Centos 7 (ISO file), and to be used with VirtualBox as provider.

**List of all yum packages installed**:

* sudo
* vim
* curl
* rsync
* wget 
* cpp 
* gcc 
* make 
* bzip2 
* perl 
* openssh-clients
* openssl-devel
* readline-devel
* zlib-devel
* kernel-headers
* kernel-devel
* net-tools
* epel-release
* yum-utils
* libselinux-python 
* elfutils-libelf-devel 
* cifs-utils 
* cloud-init

To generate the corresponding Vagrant box, use the command
```
packer build box-config.json
```

As results, a vagrant box will be generated under the builds folder.


**Keywords**: Centos7, VirtualBox, Vagrant, Packer
